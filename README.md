# Siesta electron phonon coupling utility

## 1.0 Installation

Download the repository and install the following dependencies:

    - [sisl](https://github.com/zerothi/sisl/) (the main branch is still missing the new `vectorsSileSiesta` which is used to read vibra output. Should be available in `main` soon. Until then we have to use this version: [https://github.com/nils-wittemeier/sisl/tree/add-vectorsSileSiesta](https://github.com/nils-wittemeier/sisl/tree/add-vectorsSileSiesta))
    - [numpy](https://numpy.org/)
    - [scipy](https://scipy.org/)

```
    git clone https://gitlab.com/nils.wittemeier/siesta-el-ph-utility.git
    cd siesta-el-ph-utility
    pip install .
```

For now, I recommend also adding the `-e` option to `pip install` so that the package can be automatically without needing to re-install, simply by fetching the latest developments from git.

## 1.1 Usage: inside python script / jupyter notebook 
```
import elphy # Import our elphy library
options = elphy.read_options('input.json') # Process the input file. 

elphy.run(options) # Calculate the electron phonon coupling. See output files.

```

## 1.2 Command line usage

```
> python3 run.py -h
usage: run.py [-h] [--logfile LOGFILE] [--use-tqdm | --no-use-tqdm] [--cache-dHdR | --no-cache-dHdR] [--cache-wfs | --no-cache-wfs] input

Electron-phonon utility

positional arguments:
  input                 json file with user options (command line arguments passed before this flag will be overwritten, arguments passed after this will overwrite options from the file)

options:
  -h, --help            show this help message and exit
  --logfile LOGFILE     log file, can be used to restart
  --use-tqdm, --no-use-tqdm
                        Whether to show a tqdm progress bar during the calculation.
  --cache-dHdR, --no-cache-dHdR
                        Whether to cache derivatives dH/dR.
  --cache-wfs, --no-cache-wfs
                        Whether to cache derivatives dH/dR.
```

alternatively the module can be directly executed

```
python -m elphy input.json
```


## 1.3 Input file
Expected to be in [JSON format](https://en.wikipedia.org/wiki/JSON#Syntax).

 Field            | Type       | Description 
------------------|------------|-------------
 `vibra_input`*   | `string`   | Input file used for vibra run.
 `k_sampling`*    | `dict`     | Specification of reciprocal space sampling (see section 1.3.1)
 `use_tqdm`       | `boolean`  | Whether to show a tqdm progress bar during the calculation. (Default: `true`)
 `cache_dHdR`     | `boolean`  | Whether to cache derivatives dH/dR. If `true` all derivatives are held in memory, if `false` derivatives are read from `SystemLabel-dHdR.nc` when they required. (Default: `true`)
 `cache_wfs`      | `boolean`  | Whether to cache derivatives dH/dR. If `true` wavefunctions are calculated exactly once for each wavevector $k$ and $k+q$ requiring a total of $n_k+n_k n_q$ diagonalization. If `false`, diagonalization are repeatedly carried out for each displaced atom and each displacement direction requiring a total of $n_k+3 n_{atms} n_k n_q$ diagonalization. (Default: `true`)


(*) required field


### 1.3.1 Reciprocal space sampling

 Field            | Type       | Description 
------------------|------------|-------------
 mode             | `string`   | Type of reciprocal space sampling either: [`BandStructure`](https://sisl.readthedocs.io/en/latest/api/generated/sisl.physics.BandStructure.html), [`MonkhorstPack`](https://sisl.readthedocs.io/en/latest/api/generated/sisl.physics.MonkhorstPack.html), [`BrillouinZone`](https://sisl.readthedocs.io/en/latest/api/generated/sisl.physics.BrillouinZone.html) [`Circle`](https://sisl.readthedocs.io/en/latest/api/generated/sisl.physics.BrillouinZone.html#sisl.physics.BrillouinZone.param_circle)
 kwargs           | `dict`     | Parameters for the sampling method (see below). For a complete list refer to the [sisl documentation](https://zerothi.github.io/sisl/index.html))

## 1.4 Required files

All files should be in the directory where `vibra_input` is found. 

1. `vibra_input.fdf` vibra input file
2. `SystemLabel.HSX` unperturbed Hamiltonian
3. `SystemLabel.vectors` phonon eigenmodes
2. `SystemLabel-{ia}-{idir}.HSX` files produced by SIESTA during FC run.

## 1.4 Output files

1. `SystemLabel-dHdR.nc` netCDF file containing:
    - The number of displaced atoms `na_fc`
    - The unperturbed super cell Hamiltonian and overlap matrix in sparse matrix format. Hamiltonian matrix elements are stored in Ry.
    - The unperturbed super cell geometry (including basis information)
    - The derivatives of the Hamiltonian and overlap matrix with respect to atomic displacements:
        $$\partial H/ \partial R_{I,\alpha} \quad \text{and} \quad \partial S/ \partial R_{I,\alpha} \quad I\,\text{atomic index},\quad\alpha\in{\text{x,y,z}}$$
      in sparse matrix format. Hamiltonian matrix elements are stored in Ry.
2. `SystemLabel-gElPh.nc` netCDF file containing:
    - List of $k$ points in cartesian coordinates in [1/Bohr].
    - List of $q$ points in cartesian coordinates in [1/Bohr].
    - Electron eigenenergies for every $k$ in [eV]. 
    - Phonon eigenenergies for every $q$ in [eV]. 
    - Electron phonon coupling matrix elements $g^\nu_{n,m}$ in eV ($\nu$ phonon band index, $n,m$ electron band indices). 