from __future__ import annotations

import numpy as np
from scipy.linalg import inv
from sisl import BrillouinZone, Geometry
from sisl.physics import Hamiltonian
from tqdm.auto import tqdm

from ..io.fc import dHdRncSile
from ..io.phonons import *
from ..utils.checkpoint import CheckPointTracker
from ..utils.helper import get_brillouin_zone_obj


def calc_elph_coupling(
    uc_geometry: Geometry,
    H0_uc: Hamiltonian,
    progress: CheckPointTracker,
    options: dict,
):

    # Open file containing phonon modes
    ph_modes = load_phonons(options["fdfsile"], uc_geometry)
    n_ph = ph_modes[0].state.shape[0]
    bz_q = BrillouinZone(H0_uc, list(mode.info["k"] for mode in ph_modes))

    # Determine problem size
    nsc = np.product(2 * options["sc_size"] + 1)
    no_uc = H0_uc.no
    no_sc = no_uc * nsc

    # Lattice vectors to each cell in the supercell [reciprocal units]
    R_sc = (
        np.indices(tuple(2 * options["sc_size"] + 1))
        - options["sc_size"][:, None, None, None]
    ).reshape(3, nsc)

    # Create BZ sampling for k and q points
    bz_k = get_brillouin_zone_obj(H0_uc, **options["k_sampling"])

    with tqdm(
        total=len(bz_k) * len(bz_q) * len(uc_geometry) * 3,
        desc="Electron phonon coupling",
        bar_format="{desc}: {percentage:3.0f}%|{bar}|[{elapsed}<{remaining}]",
    ) as pbar:

        g_nc = ElectronPhononCouplingSile(
            options["fdfsile"].dir_file(
                options["fdfsile"].get("SystemLabel", default="siesta") + "-gElPh.nc"  # type: ignore
            ),
            mode="a",
        )
        g_nc.write_geometry(uc_geometry)
        g_nc.write_k(bz_k)
        g_nc.write_q(bz_q)
        g_nc.write_hw(np.asarray(list(mode.c for mode in ph_modes)))

        dHdR_nc = dHdRncSile(options["dHdR_nc_fname"], mode="r")
        if options["cache_dHdR"]:
            dHSdR = load_all_dHSdR(dHdR_nc, uc_geometry.na, no_sc)

        eigs = np.empty((len(bz_k), no_uc), dtype=np.float64)

        for ik, k in enumerate(bz_k):
            # for ik, k in enumerate(bz_k):
            # k phase
            eikR = np.exp(np.pi * 2j * np.dot(k, R_sc))
            # Determine electronic eigenstate at k
            states = H0_uc.eigenstate(k)
            ck = states.state.T
            eigs[ik, :] = states.c

            # Prepare products of Hk and inv(Sk) - we can reuse these for each displacement/direction
            Hk = H0_uc.Hk(k, format="array")
            invSk = inv(H0_uc.Sk(k, format="array"))
            HkinvSk = np.matmul(Hk, invSk)  # type: ignore
            invSkHk = np.matmul(invSk, Hk)  # type: ignore

            g = np.zeros((len(bz_q), n_ph, no_uc, no_uc), dtype=np.complex128)
            dHdR_k = np.full((nsc, no_uc, no_uc), np.nan, dtype=np.complex128)
            dHdR_kq = np.full((no_uc, no_uc), np.nan, dtype=np.complex128)

            if options["cache_wfs"]:
                bz_kpq = BrillouinZone(H0_uc, bz_q.k + k)
                c_cache = bz_kpq.apply.array.eigenstate(
                    eta=False,
                    wrap=lambda e: np.conj(e.state),
                    dtype=np.complex128,
                )

            for ia in uc_geometry:
                for ixyz in range(3):
                    if options["cache_dHdR"]:
                        dHdR = dHSdR[ia, ixyz, :, :, 0].reshape(nsc, no_uc, nsc, no_uc)
                        dSdR = dHSdR[ia, ixyz, :, :, -1].reshape(nsc, no_uc, nsc, no_uc)
                    else:
                        dHSdR = load_dHSdR(dHdR_nc, no_sc, ia, ixyz)
                        dHdR = dHSdR[:, :, 0].reshape(nsc, no_uc, nsc, no_uc)
                        dSdR = dHSdR[:, :, -1].reshape(nsc, no_uc, nsc, no_uc)

                    # Multiply with k-phase and reduce over second lattice vector dimension
                    # The axes key words allows us to perform MM over the correct
                    dHdR_k = np.matmul(dHdR, eikR, axes=[(0, 2), 0, 0])  # type: ignore
                    dSdR_k = np.matmul(dSdR, eikR, axes=[(0, 2), 0, 0])  # type: ignore
                    # Result: dHdR_k.shape = (nsc, no_uc, no_uc)

                    # Add the contributiosn
                    #   dS @ invSk @ Hk  and Hk @ invSk @ dS
                    dHdR_k += np.matmul(dSdR_k, invSkHk)
                    dHdR_k += np.matmul(HkinvSk, dSdR_k)

                    # Multiply with the eigenstate at k along last dimension
                    dHdR_k = np.matmul(dHdR_k, ck)
                    # Result: dHdR_k.shape = (nsc, no_uc, n_bands)

                    for iq, q in enumerate(bz_q):
                        eikpqR = np.exp(-np.pi * 2j * np.dot(k + q, R_sc))
                        if options["cache_wfs"]:
                            ckpq = c_cache[iq]
                        else:
                            ckpq = np.conj(H0_uc.eigenstate(k + q).state)

                        #  Multiply with k+q-phase and reduce over first lattice vector dimension
                        dHdR_kq = np.matmul(eikpqR, dHdR_k, axes=[0, (0, 1), 0])  # type: ignore
                        # Result: dHdR.shape = (no_uc, n_bands)

                        # Multiply with the eigenstate at k along first uc_orb dimension
                        dHdR_kq = np.matmul(ckpq, dHdR_kq)
                        # Result: dHdR.shape = (n_bands, n_bands)

                        g[iq] += (
                            dHdR_kq[None, :, :]
                            * ph_modes[iq].state.reshape(
                                (n_ph, 3, len(uc_geometry), 1, 1)
                            )[:, ixyz, ia]
                        )
                        # shape (n_ph_modes, n_bands, n_bands)
                        pbar.update(1)

            g_nc.write_g(ik, g)

    g_nc.write_eigs(eigs)
    g_nc.close()
