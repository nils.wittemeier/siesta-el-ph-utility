"""
Physics
=======

This module includes algorithms for calculating the following physical
quantities:

    - dH/dR: derivative of the Hamiltonian with respect to atomic positions
             using finite differences
    - g: electron phonon coupling matrix using dHR


"""
