from __future__ import annotations

import numpy as np
from sisl import Geometry
from sisl.physics import Hamiltonian
from tqdm.auto import tqdm

from ..io.fc import dHdRncSile, read_HS_fc
from ..utils.checkpoint import STATE, CheckPoint, CheckPointTracker
from ..utils.helper import uc_offset

np.set_printoptions(edgeitems=30, linewidth=100000)


def calc_dRdH(
    geometry: Geometry,
    H0: Hamiltonian,
    progress: CheckPointTracker,
    options: dict,
) -> dHdRncSile:
    """Calculate the derivative of the Hamiltonian w.r.t to atomic positions

    Arguments:
    ----------
    H0
        Unperturbed Hamiltonian calculated in the super cell
    geometry
        Unperturbed unit cell geometry
    options
        Dictionary with all calculation options
    progress
        Object tracking progress of the current calculation, which can be used to determine
        from which point to restart.

    """

    if "dHdR" not in progress:
        progress.add(
            "dHdR",
            CheckPoint(
                "Calculating derivative of Hamiltonian w.r.t atomic positions.",
                STATE.INIT,
            ),
        )

    options["dHdR_nc_fname"] = options["fdfsile"].dir_file(
        options["fdfsile"].get("SystemLabel", "siesta") + "-dHdR.nc"
    )

    dHdR_nc = dHdRncSile(options["dHdR_nc_fname"], mode="a")

    dHdR_nc.init_dHdR(H0, geometry.na)

    progress["dHdR"].update(STATE.RUNNING, completion=0.0)

    uc_off = uc_offset(options["sc_size"])

    with tqdm(
        total=len(geometry) * 3,
        desc="Finite derivative",
        bar_format="{desc}: {percentage:3.0f}%|{bar}|[{elapsed}<{remaining}]",
    ) as pbar:

        for ia_uc in geometry:
            ia = ia_uc + geometry.na * uc_off
            for idir, direction in enumerate(["x", "y", "z"]):
                dHdR = read_HS_fc(options["fdfsile"], "+" + direction, ia, geometry)
                cp_key = f"{ia}-{direction}"
                if cp_key in progress["dHdR"]:
                    if progress["dHdR"][cp_key].state == STATE.COMPLETE:
                        pass
                        # continue
                else:
                    progress["dHdR"].add(
                        f"{ia}-{direction}",
                        CheckPoint(f"[atm={ia},dir={direction}]", STATE.RUNNING),
                    )

                if not dHdR.spsame(H0):
                    raise RuntimeError(
                        f"calc_dRdH: sparsity pattern changed for displacement of atom {ia+1} in "
                        f"direction '-{direction}'."
                    )

                tmp = read_HS_fc(options["fdfsile"], "-" + direction, ia, geometry)

                if not dHdR.spsame(H0):
                    raise RuntimeError(
                        f"calc_dRdH: sparsity pattern changed for displacement of atom {ia+1} in "
                        f"direction '+{direction}'."
                    )
                dHdR._csr._D -= tmp._csr._D
                dHdR._csr._D /= 2 * options["dR"]

                dHdR_nc.write_dHdR(dHdR, ia_uc, idir)

                progress["dHdR"][f"{ia}-{direction}"].update(STATE.COMPLETE)
                progress["dHdR"].update(
                    STATE.RUNNING, completion=(ia_uc + 1) / geometry.na
                )
                pbar.update()

    progress["dHdR"].update(STATE.COMPLETE)
    progress.write()

    dHdR_nc.close()
