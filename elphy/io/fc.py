from __future__ import annotations

import numpy as np
from sisl._core.sparse import _ncol_to_indptr
from sisl.io import ncSileSiesta
from sisl.io.siesta import hsxSileSiesta
from sisl.io.siesta._help import _csr_to_siesta, _mat_spin_convert
from sisl.io.sile import SileError
from sisl.physics import Hamiltonian
from sisl.unit.siesta import unit_convert

Ry2eV = unit_convert("Ry", "eV")

_dir_int = {
    "-x": 1,
    "+x": 2,
    "-y": 3,
    "+y": 4,
    "-z": 5,
    "+z": 6,
}


def read_HS_fc(base_sile, direction, iatom, geometry):
    idisp = _dir_int[direction]

    fname = base_sile.dir_file(
        base_sile.get("SystemLabel", default="siesta")
        + f".{iatom+1:05d}-{idisp:1d}.HSX"
    )

    return hsxSileSiesta(fname).read_hamiltonian(geometry=geometry)


def read_sc_size(sile):
    return np.asarray([sile.get(f"SuperCell_{i+1}") for i in range(3)])


class dHdRncSile(ncSileSiesta):

    def init_dHdR(self, H0, na_fc):
        """ """
        self._crt_dim(self, "na_fc", na_fc)
        self.write_hamiltonian(H0)

    def write_dHdR(self, dHdR: Hamiltonian, ia: int, idir: int, **kwargs):
        """Writes one derivative of the Hamiltonian to file

        Parameters
        ----------
        dHdR
           the derivative to be saved in the NC file
        ia
            index of displaced atom
        idir
            direction of displacement {0: x, 1: y, 2: z}
        """

        csr = dHdR.transpose(spin=False, sort=False)._csr
        if csr.nnz == 0:
            raise SileError(
                f"{self}.write_hamiltonian cannot write a zero element sparse matrix!"
            )

        # Convert to siesta CSR
        _csr_to_siesta(dHdR.geometry, csr)
        csr.finalize(sort=kwargs.get("sort", True))
        _mat_spin_convert(csr, dHdR.spin)

        if dHdR.dkind != "f":
            raise NotImplementedError(
                "Currently we only allow writing a floating point Hamiltonian to the Siesta format"
            )

        # Save sparse matrices
        spgroup = self.groups["SPARSE"]
        v = self._crt_var(
            spgroup,
            f"dSdR",
            "f8",
            ("na_fc", "xyz", "nnzs"),
            chunksizes=(
                1,
                1,
                len(csr.col),
            ),
            **self._cmp_args,
        )
        v.info = f"Derivative of Overlap matrix w.r.t. to atomic positions"
        v[ia, idir, :] = csr._D[:, dHdR.S_idx]

        v = self._crt_var(
            spgroup,
            f"dHdR",
            "f8",
            ("na_fc", "xyz", "spin", "nnzs"),
            chunksizes=(1, 1, 1, len(csr.col)),
            **self._cmp_args,
        )
        v.info = f"Derivative of Hamiltonian w.r.t. to atomic positions"
        v.unit = "Ry"
        for i in range(len(dHdR.spin)):  # type: ignore
            v[ia, idir, i, :] = csr._D[:, i] / Ry2eV

        self._write_settings()

    def read_dHdR(self, ia: int, idir: int, **kwargs) -> Hamiltonian:
        """Writes one derivative of the Hamiltonian to file

        Parameters
        ----------
        ia
            index of displaced atom
        dir
            direction of displacement
        """
        # Get the default spin channel
        spin = len(self._dimension("spin"))

        # First read the geometry
        geom = self.read_geometry()

        # Populate the things
        sp = self.groups["SPARSE"]
        dHdR = Hamiltonian(geom, spin, nnzpr=1, orthogonal=False)

        dHdR._csr.ncol = np.array(sp.variables["n_col"][:], np.int32)
        # Update maximum number of connections (in case future stuff happens)
        dHdR._csr.ptr = _ncol_to_indptr(dHdR._csr.ncol)
        dHdR._csr.col = np.array(sp.variables["list_col"][:], np.int32) - 1

        # Copy information over
        dHdR._csr._nnz = len(dHdR._csr.col)

        dHdR._csr._D = np.empty([dHdR._csr.ptr[-1], spin + 1], np.float64)

        if sp.variables["dHdR"].unit != "Ry":
            raise SileError(
                f"{self}.read_hamiltonian requires the stored matrix to be in Ry!"
            )

        for i in range(len(dHdR.spin)):  # type: ignore
            dHdR._csr._D[:, i] = sp.variables["dHdR"][ia, idir, i, :] * Ry2eV
        dHdR._csr._D[:, -1] = sp.variables["dSdR"][ia, idir, :]
        return dHdR
