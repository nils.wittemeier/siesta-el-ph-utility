import numpy as np
from sisl.io import ncSileSiesta
from sisl.io.siesta import fdfSileSiesta, vectorsSileSiesta
from sisl.unit.siesta import unit_convert

_Bohr2Ang = unit_convert("Bohr", "Ang")


def load_phonons(fdfsile: fdfSileSiesta, geometry):
    """Load phonon modes from vibra output <>.vectors

    Arguments:
    ----------
    fdfsile
    """

    phsile = vectorsSileSiesta(
        fdfsile.dir_file(
            fdfsile.get("SystemLabel", default="siesta") + ".vectors"  # type: ignore
        ),
        geometry=geometry,
    )

    return list(mode for mode in phsile.yield_eigenmode())


def load_all_dHSdR(dHdR_nc, na, no_sc):
    dHSdR = np.full((na, 3, no_sc, no_sc, 2), np.nan)
    for ia in range(na):
        for ixyz in range(3):
            # Read derivative from netCDF file
            sp_dHdR = dHdR_nc.read_dHdR(ia, ixyz)
            # Remove inter cell connections
            # Convert to dense matrix:
            # H -> [...,  0]
            # S -> [..., -1]
            # TODO for calculations with spin this should be done differently
            dHSdR[ia, ixyz, :, :, :] = sp_dHdR._csr.todense()[0:no_sc, 0:no_sc, 0:2]

    assert not np.isnan(dHSdR).any(), f"load_all_dHSdR: Error loading derivatives"
    return dHSdR


def load_dHSdR(dHdR_nc, no_sc, ia, ixyz):
    sp_dHdR = dHdR_nc.read_dHdR(ia, ixyz)
    # Convert to dense matrix:
    # H -> [...,  0]
    # S -> [..., -1]
    # and remove inter cell connections
    # TODO for calculations with spin this should be done differently
    dHSdR = sp_dHdR._csr.todense()[0:no_sc, 0:no_sc]
    return dHSdR


class ElectronPhononCouplingSile(ncSileSiesta):

    def write_k(self, bz_k):
        self._crt_dim(self, "nk", len(bz_k))
        self._crt_dim(self, "xyz", 3)

        v = self._crt_var(self, "k", "f8", ("nk", "xyz"), **self._cmp_args)
        v.info = "Electron wave vectors"
        v.unit = " 1 / Bohr"
        v[:, :] = bz_k.tocartesian(bz_k.k) / _Bohr2Ang

    def write_q(self, bz_q):
        self._crt_dim(self, "nq", len(bz_q))
        self._crt_dim(self, "xyz", 3)

        v = self._crt_var(self, "q", "f8", ("nq", "xyz"), **self._cmp_args)
        v.info = "Phonon wave vectors"
        v.unit = " 1 / Bohr"
        v[:, :] = bz_q.tocartesian(bz_q.k) / _Bohr2Ang

    def write_eigs(self, eigs):
        self._crt_dim(self, "n_el_bands", eigs.shape[1])

        v = self._crt_var(self, "eigs", "f8", ("nk", "n_el_bands"), **self._cmp_args)
        v.info = "Electron energies"
        v.unit = " eV"
        v[:, :] = eigs

    def write_hw(self, hw):
        self._crt_dim(self, "n_ph_bands", hw.shape[1])

        v = self._crt_var(self, "hw", "f8", ("nq", "n_ph_bands"), **self._cmp_args)
        v.info = "Phonon energies"
        v.unit = " eV"
        v[:, :] = hw

    def write_g(self, ik, g):
        """Write a component of the electron-phonon coupling

        Parameters
        ----------
        ik
            index of k point
        """
        self._crt_dim(self, "n_ph_bands", g.shape[1])
        self._crt_dim(self, "n_el_bands", g.shape[2])

        v = self._crt_var(
            self,
            "g_real",
            "f8",
            ("nk", "nq", "n_ph_bands", "n_el_bands", "n_el_bands"),
            chunksizes=(1, g.shape[0], g.shape[1], g.shape[2], g.shape[2]),
            **self._cmp_args,
        )
        v.info = "Real part electron phonon coupling matrix"
        v.unit = "eV"

        v[ik, :, :, :, :] = g[:, :, :, :].real

        v = self._crt_var(
            self,
            "g_imag",
            "f8",
            ("nk", "nq", "n_ph_bands", "n_el_bands", "n_el_bands"),
            chunksizes=(1, g.shape[0], g.shape[1], g.shape[2], g.shape[2]),
            **self._cmp_args,
        )
        v.info = "Imaginary part electron phonon coupling matrix"
        v.unit = "eV"

        v[ik, :, :, :, :] = g[:, :, :, :].imag
        v[ik, :, :, :, :] = g[:, :, :, :].imag
