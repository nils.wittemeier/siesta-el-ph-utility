from sisl import BandStructure, BrillouinZone, MonkhorstPack


def create_supercell_geometry(geometry, sc_size):
    """Create super cell geometry the same way SIESTA does

    This involves tiling the geometry and shifting it so that the
    origin of the unit cell is equal to the to origin of the central cell
    in the super cell geometry.

    Arguments:
    ----------
    geometry : sisl.Geometry
        Unit cell geometry to be expanded to super cell.
    sc_size : array_like of int of shape(3,)
        Super cell parameters as specified in SIESTA input.
    """

    sc_geometry = geometry.copy()
    for i in range(3):
        sc_geometry = sc_geometry.tile(2 * sc_size[i] + 1, i)
    return sc_geometry.move(-sc_size @ geometry.lattice.cell)


def uc_offset(sc_size):
    """Determine the offset a unit cell orbitals within the super cell.

    Arguments:
    ----------
    geometry : sisl.Geometry
        Unit cell geometry to be expanded to super cell.
    sc_size : array_like of int of shape(3,)
        Super cell parameters as specified in SIESTA input.

    """

    return (
        4 * sc_size[0] * sc_size[1] * sc_size[2]
        + 2
        * (sc_size[0] * sc_size[1] + sc_size[1] * sc_size[2] + sc_size[2] * sc_size[0])
        + (sc_size[0] + sc_size[1] + sc_size[2])
    )


def get_brillouin_zone_obj(parent, mode, **kwargs):
    """Retrieve BrillouinZone object from sisl"""
    if mode == "BandStructure":
        return BandStructure(parent, **kwargs)
    elif mode == "MonkhorstPack":
        return MonkhorstPack(parent, **kwargs)
    elif mode == "BrillouinZone":
        return BrillouinZone(parent, **kwargs)
    elif mode == "Circle":
        return BandStructure.param_circle(parent, **kwargs)
    else:
        assert False, (
            f'Unknown kpath mode "{mode}". Allowed modes are: '
            + '"BandStructure", "MonkhorstPack", "BrillouinZone", "Circle"'
        )
