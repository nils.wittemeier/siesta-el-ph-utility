import json
from datetime import datetime
from pathlib import Path

_state_code = {
    "failed": -1,
    "initializing": 0,
    "running": 1,
    "completed": 2,
    "skipped": 3,
}

_state_str = {
    -1: "failed",
    0: "initializing",
    1: "running",
    2: "completed",
    3: "skipped",
}


class STATE:
    FAIL = -1
    INIT = 0
    RUNNING = 1
    COMPLETE = 2
    SKIPPED = 3


class CheckPoint:

    def __init__(self, name, state, history=None, completion=None, substep=None):
        self.name = name
        self.history = dict() if history is None else history
        self.completion = completion
        self.update(state, completion)
        if substep is None:
            self.substep = dict()
        else:
            self.substep = {t: CheckPoint(**substep[t]) for t in substep}

    def update(self, state, completion=None):
        if type(state) == int:
            if state not in _state_str:
                raise ValueError(
                    f"{self.__class__.__name__}.update: {state} is not a valid state."
                )
            self.state = state
        else:
            self.state = _state_code[state]
        if completion is not None:
            self.history[f"{_state_str[self.state].upper()} [{100*completion}%]"] = (
                datetime.now()
            )
        else:
            self.history[f"{_state_str[self.state].upper()}"] = datetime.now()

    def __getitem__(self, tag):
        return self.substep[tag]

    def __contains__(self, tag):
        return tag in self.substep

    def __dict__(self):
        d = {
            "name": self.name,
            "state": _state_str[self.state],
        }
        if len(self.history) > 0:
            d["history"] = self.history
        if self.completion is not None:
            d["completion"] = self.completion
        if len(self.substep) > 0:
            d["substep"] = self.substep
        return d

    def add(self, tag, checkpoint):
        if tag in self.substep:
            raise IndexError(
                f"{self.__class__.__name__}.add: checkpoint with tag '{tag} already exists."
            )
        else:
            self.substep[tag] = checkpoint


class CheckPointTracker:

    def __init__(self, file):
        if isinstance(file, str):
            self.file = open(file, "a+")
        else:
            self.file = file

        data = self.load(self.file)

        if data is None:
            self.checkpoints = {}
        else:
            self.checkpoints = {tag: CheckPoint(**data[tag]) for tag in data}

    def load(self, file):
        self.file.seek(0)
        try:
            return json.load(self.file)
        except json.decoder.JSONDecodeError:
            # raise RuntimeWarning(
            print(
                f"{self.__class__.__name__}.load: unable to parse log file '{self.file.name}'."
                "Restart from scratch."
            )

    def add(self, tag, checkpoint):
        if tag in self.checkpoints:
            raise IndexError(
                f"{self.__class__.__name__}.add: checkpoint with tag '{tag} already exists."
            )
        else:
            self.checkpoints[tag] = checkpoint

    def __getitem__(self, tag):
        return self.checkpoints[tag]

    def __contains__(self, tag):
        return tag in self.checkpoints

    def write(self):
        self.file.seek(0)
        self.file.truncate()
        json.dump(self.checkpoints, self.file, cls=CheckPointEncoder, indent=4)


class CheckPointEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, CheckPoint):
            return o.__dict__()
        elif isinstance(o, datetime):
            return o.__str__()
        else:
            o.__dict__
