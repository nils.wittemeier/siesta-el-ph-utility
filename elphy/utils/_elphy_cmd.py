from __future__ import annotations


def create_parser():
    """Parse command line arguments

    Returns
    -------
    args: dict
      all script option passed via command line and inputfile
    """

    import argparse
    import json
    from typing import TYPE_CHECKING

    if TYPE_CHECKING:
        from _typeshed import SupportsRead

    class LoadFromFile(argparse.Action):
        def __call__(
            self,
            parser: argparse.ArgumentParser,
            namespace: argparse.Namespace,
            fp: SupportsRead[str | bytes],
            option_string: str | None = None,
        ):
            config = json.load(fp)
            for key in config:
                setattr(namespace, key, config[key])

    parser = argparse.ArgumentParser(description="Electron-phonon utility")

    parser.add_argument(
        "input",
        type=open,
        action=LoadFromFile,
        help="json file with user options  (command line arguments passed "
        "before this flag will be overwritten, arguments passed "
        "after this will overwrite options from the file)",
    )

    parser.add_argument(
        "--logfile",
        type=str,
        help="log file, can be used to restart",
        default="elphy.log.json",
    )

    parser.add_argument(
        "--use-tqdm",
        action=argparse.BooleanOptionalAction,
        help="Whether to show a tqdm progress bar during the calculation.",
        default=True,
    )

    parser.add_argument(
        "--cache-dHdR",
        action=argparse.BooleanOptionalAction,
        help="Whether to cache derivatives dH/dR.",
        default=True,
    )

    parser.add_argument(
        "--cache-wfs",
        action=argparse.BooleanOptionalAction,
        help="Whether to cache derivatives dH/dR.",
        default=True,
    )
    return parser


def read_options(*args, **kwargs):
    """Get options from command line and input file"""
    from argparse import FileType

    from sisl.io.siesta import fdfSileSiesta

    from elphy.io.fc import read_sc_size

    parser = create_parser()
    cl_args = []
    cwd = kwargs.pop("cwd", None)

    for key in kwargs:
        cl_args.append(key)
        cl_args.append(kwargs[key])

    if len(args) > 0:
        if cwd is not None:
            cl_args.append(str(cwd / args[0]))
        else:
            cl_args.append(args[0])

        for arg in args[1:]:
            cl_args.append(arg)

    if len(cl_args) > 0:
        options = vars(parser.parse_args(cl_args))
    else:
        options = vars(parser.parse_args())

    if cwd is None:
        fdfsile = fdfSileSiesta(options["vibra_input"])
        logfile = FileType("a+")(options["logfile"])
    else:
        # Ensure files are opened/stored in the current working directory
        fdfsile = fdfSileSiesta(cwd / options["vibra_input"])
        logfile = FileType("a+")(cwd / options["logfile"])

    options |= dict(
        fdfsile=fdfsile,
        logfile=logfile,
        SystemLabel=fdfsile.get("SystemLabel", "siesta"),
        sc_size=read_sc_size(fdfsile),
        dR=fdfsile.get(
            "MD.FCdispl", fdfsile.get("AtomicDispl", unit="Ang"), unit="Ang"
        ),
    )

    assert options["dR"] is not None, "Can not read MD.FCdispl from input file!"
    assert (
        options["sc_size"] != None
    ).all(), "Can not read SuperCell size at least one value missing."
    return options


def elphy_cmd():
    from ..main import run

    # Parse command line arguments and parse input file
    options = read_options()

    run(options)
