def run(options):
    from .physics.elph_coupling import calc_elph_coupling
    from .physics.finite_differences import calc_dRdH
    from .utils.checkpoint import CheckPointTracker
    from .utils.helper import create_supercell_geometry

    progress = CheckPointTracker(options["logfile"])

    geometry = options["fdfsile"].read_geometry(order="fdf")
    H0 = options["fdfsile"].read_hamiltonian(order="HSX")
    H0_uc = (
        H0.untile(2 * options["sc_size"][0] + 1, 0)
        .untile(2 * options["sc_size"][1] + 1, 1)
        .untile(2 * options["sc_size"][2] + 1, 2)
    )
    sc_geometry = create_supercell_geometry(geometry, options["sc_size"])

    calc_dRdH(geometry, H0, progress, options)

    calc_elph_coupling(geometry, H0_uc, progress, options)
